//
//  Message.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import Foundation

struct Message: Identifiable{
    var id: Int
    var imageName: String
    var publisher: String
    var date: String
    var message: String
}
