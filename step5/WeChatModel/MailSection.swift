//
//  MailSection.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/4.
//

import SwiftUI

struct MailSection: View {
    
    var color: Color = Color.orange
    
    var text: String
    
    var body: some View {
        VStack(alignment: .leading){
            HStack{
                ZStack{
                    Rectangle().fill().cornerRadius(5).foregroundColor(color).aspectRatio(1, contentMode: .fit).frame(height: 50)
                    Image(systemName: "person.2.circle").foregroundColor(.white).font(.system(size: 40))
                }.padding(.trailing)
                Text(text)
                
            }
            Divider()
        }.padding()
    }
}

struct MailSection_Previews: PreviewProvider {
    static var previews: some View {
        MailSection(color: Color.orange, text: "asdasd")
    }
}
