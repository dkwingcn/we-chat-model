//
//  MessageChatView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/2.
//

import SwiftUI

struct MessageChatView: View {
    
    var title: String
    
    @State var text: String = ""
    
    var body: some View {
        
        ZStack(alignment: .bottomLeading){
            ScrollView{
                
                HStack{
                    Spacer()
                    ChatBubble(text: "你好")
                }.padding()
                
                HStack{
                    Spacer()
                    ChatBubble(text: "有事吗")
                }.padding()
                
                HStack{
                    Spacer()
                    ChatBubble(text: "有事吗")
                }.padding()
                
                HStack{
                    Spacer()
                    ChatBubble(text: "有事吗")
                }.padding()
                
            }.navigationTitle(title).navigationBarItems(trailing: Image(systemName: "person"))
            
            
            ZStack(alignment: .bottomLeading){
                Rectangle().fill().foregroundColor(.white)
                HStack{
                    
                    Image(systemName: "text.bubble").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    Image(systemName: "waveform.circle").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                                    
                    TextField("", text: $text).textFieldStyle(RoundedBorderTextFieldStyle())
                                    
                    Image(systemName: "face.smiling").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    
                    Image(systemName: "plus.circle").font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                }.padding()
            }.frame(height: 50)
            
           
        }
        
        
    }
}

struct MessageChatView_Previews: PreviewProvider {
    static var previews: some View {
        MessageChatView(title: "海底捞火锅")
    }
}
