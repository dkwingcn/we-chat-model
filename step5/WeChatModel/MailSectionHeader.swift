//
//  MailSectionHeader.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/4.
//

import SwiftUI

struct MailSectionHeader: View {
    var title: String
    var body: some View {
        HStack{
          Text(title)
            Spacer()
        }.padding().background(Color(red: 237/255, green: 237/255, blue: 237/255))
    }
}

struct MailSectionHeader_Previews: PreviewProvider {
    static var previews: some View {
        MailSectionHeader(title: "A")
    }
}
