//
//  MessageView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

struct MessageView: View {
    
    
    var imageName: String
    
    var publisher: String
    
    var date: String
    
    var message: String
    
    var body: some View {
        HStack{
            Image(imageName).resizable().scaledToFill().frame(width: 75, height: 75).cornerRadius(10).clipped().padding()
            VStack(alignment: .leading){
                HStack{
                    Text(publisher).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/).font(.title3)
                    Spacer()
                    Text(date).padding().foregroundColor(.gray)
                }
                Text(message).foregroundColor(.gray).font(.callout).bold()
                Divider()
            }
        }
    }
}

struct MessageView_Previews: PreviewProvider {
    static var previews: some View {
        MessageView(imageName: "dzdp", publisher: "大众点评", date: "2021/7/31", message: "必吃榜发布!解密本地人爱吃的10碗面")
    }
}
