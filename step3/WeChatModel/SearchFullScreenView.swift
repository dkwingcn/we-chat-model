//
//  SearchFullScreenView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/3.
//

import SwiftUI

struct SearchFullScreenView: View {
    
    @State var searchText: String = ""
    
    @Binding var searchFullPagePresented: Bool
    
    var commonSearchFilter: [String] = [
        "朋友圈","文章","公众号","小程序","音乐","表情","服务"
    ]
    
    var body: some View {
        VStack{
            HStack{
                ZStack{
                    Rectangle().fill().frame(height: 50).foregroundColor(.white).cornerRadius(5).padding(12.5)
                    
                    HStack{
                        
                        Image(systemName: "magnifyingglass")
                        TextField("搜索", text: $searchText)
                        Image(systemName: "mic")
                        
                    }.padding()
                }
                Button("取消") {
                    searchFullPagePresented = false
                }.padding(.trailing)
            }.background(Color(red: 237/255, green: 237/255, blue: 237/255))
            
            
            Text("指定搜索内容").foregroundColor(.gray).font(.title3).padding(.vertical,50)
            
            
            let columns: [GridItem] =
                     Array(repeating: .init(.flexible()), count: 3)
            LazyVGrid(columns: columns) {
                ForEach((0..<commonSearchFilter.count), id: \.self) {index in
                    
                    Text("\(commonSearchFilter[index])").font(.callout).padding(.bottom).foregroundColor(.blue)
                   
                }
            }.font(.largeTitle)
            
            Spacer()
        }
        
    }
}

struct SearchFullScreenView_Previews: PreviewProvider {
    static var previews: some View {
        SearchFullScreenView(searchFullPagePresented: .constant(false))
    }
}
