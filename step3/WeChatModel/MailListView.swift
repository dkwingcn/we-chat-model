//
//  MailListView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

struct MailListView: View {
    
    @State var searchFullPagePresented: Bool = false
    
    
    var body: some View {
        
        ScrollView{
            ZStack{
                
                Rectangle().fill().frame(height: 50).foregroundColor(.white).cornerRadius(5).padding(12.5)
                
                HStack{
                    Image(systemName: "magnifyingglass")
                    Text("搜索")
                }.foregroundColor(Color(red: 188/255, green: 188/255, blue: 188/255))
                
                
            }.fullScreenCover(isPresented: $searchFullPagePresented, content: {
                SearchFullScreenView(searchFullPagePresented: $searchFullPagePresented)
            }).onTapGesture {
                searchFullPagePresented = true
            }
        }.background(Color(red: 237/255, green: 237/255, blue: 237/255))
        
        
    }
}

struct MailListView_Previews: PreviewProvider {
    static var previews: some View {
        MailListView()
    }
}
