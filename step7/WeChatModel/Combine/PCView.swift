//
//  PCView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/6.
//

import SwiftUI

struct PCView: View {
    
    @EnvironmentObject var model: AnimalViewModel
    
    var body: some View {
        List{
            Text("PCView").background(Color.green).foregroundColor(.white)
            Button(action: {
                model.animals.append("cat")
            }, label: {
                Text("addAnimal")
            })
            ForEach(0..<model.animals.count, id: \.self){index in
                Text(model.animals[index])
            }
        }
    }
}

struct PCView_Previews: PreviewProvider {
    static var previews: some View {
        PCView()
    }
}
