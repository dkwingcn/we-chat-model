//
//  TVView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/6.
//

import SwiftUI

struct TVView: View {
    
    @EnvironmentObject var model: AnimalViewModel
    
    var body: some View {
        
        List{
            Text("TVView").background(Color.blue).foregroundColor(.white)
            Button(action: {
                model.animals.append("cat")
            }, label: {
                Text("addAnimal")
            })
            ForEach(0..<model.animals.count, id: \.self){index in
                Text(model.animals[index])
            }
        }
    }
}

struct TVView_Previews: PreviewProvider {
    static var previews: some View {
        TVView()
    }
}
