//
//  ChatListView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

struct ChatListView: View {
    
    let messages: [Message] = [
        Message(id: 0, imageName: "jd", publisher: "京东JD.COM", date: "2021/7/31", message: "交易完成通知"),
        Message(id: 1, imageName: "hdl", publisher: "海底捞火锅", date: "2021/7/31", message: "这碗胡辣汤，可得劲儿！"),
        Message(id: 2, imageName: "dzdp", publisher: "大众点评", date: "2021/7/31", message: "必吃榜发布!解密本地人爱吃的10碗面")
    ]
    
    var body: some View {
        
        VStack{
            ForEach(messages){message in
                MessageView(imageName: message.imageName, publisher: message.publisher, date: message.date, message: message.message)
                
            }
        }
        
        
    }
}

struct ChatListView_Previews: PreviewProvider {
    static var previews: some View {
        ChatListView()
    }
}
