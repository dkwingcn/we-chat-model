//
//  ContentView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            
            NavigationView{
                ChatListView()
                    .navigationTitle("微信").navigationBarItems(leading: Text("· ·"), trailing: Image(systemName: "plus.circle")).navigationBarTitleDisplayMode(.inline)
            }.tabItem {
                Image(systemName: "message.fill")
                Text("微信")
            }
            MailListView()
                .tabItem {
                    Image(systemName: "person.2.fill")
                    Text("通讯录")
                }
            ExploreView()
                .tabItem {
                    Image(systemName: "location.circle")
                    Text("发现")
                }
            MineView()
                .tabItem {
                    Image(systemName: "person")
                    Text("我")
                }
        }
       
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
