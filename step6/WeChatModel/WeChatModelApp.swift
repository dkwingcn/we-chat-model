//
//  WeChatModelApp.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

@main
struct WeChatModelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(AnimalViewModel())
        }
    }
}
