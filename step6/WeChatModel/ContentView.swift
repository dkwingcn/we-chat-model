//
//  ContentView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            
            TVView().tabItem {
                Image(systemName: "pencil.circle")
                Text("TVView")
            }
            PCView().tabItem {
                Image(systemName: "pencil.circle")
                Text("TVView")
            }
        }
       
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
