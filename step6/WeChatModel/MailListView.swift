//
//  MailListView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/1.
//

import SwiftUI

struct MailListView: View {
    
    @State var searchFullPagePresented: Bool = false
    
    @State var wordNavigationtappedIndex: Int = -1
    
    
    var body: some View {
        
        ZStack{
            ScrollView{
                ScrollViewReader{value in
                    LazyVStack(pinnedViews: [.sectionHeaders, .sectionFooters]){
                        ZStack{
                            
                            Rectangle().fill().frame(height: 50).foregroundColor(.white).cornerRadius(5).padding(12.5)
                            
                            HStack{
                                Image(systemName: "magnifyingglass")
                                Text("搜索")
                            }.foregroundColor(Color(red: 188/255, green: 188/255, blue: 188/255))
                            
                            
                        }.fullScreenCover(isPresented: $searchFullPagePresented, content: {
                            SearchFullScreenView(searchFullPagePresented: $searchFullPagePresented)
                        }).onTapGesture {
                            searchFullPagePresented = true
                        }.background(Color(red: 237/255, green: 237/255, blue: 237/255))
                        Section(header: MailSectionHeader(title: "A").background(Color.red)){
                            MailSection(text: "Apple")
                            MailSection(text: "Aaron")
                        }.id(0)
                        
                        Section(header: MailSectionHeader(title: "B").background(Color.red)){
                            MailSection(text: "Black")
                            MailSection(text: "Bad")
                        }.id(1)
                        
                        Section(header: MailSectionHeader(title: "C").background(Color.red)){
                            MailSection(text: "Cat")
                        }.id(2)
                        
                        Section(header: MailSectionHeader(title: "D").background(Color.red)){
                            MailSection(text: "D1")
                            MailSection(text: "D2")
                            MailSection(text: "D3")
                            MailSection(text: "D4")
                            MailSection(text: "D5")
                        }.id(3)
                        
                        Section(header: MailSectionHeader(title: "E").background(Color.red)){
                            MailSection(text: "Eason")
                        }.id(4)
                    }.onChange(of: wordNavigationtappedIndex, perform: { index in
                        value.scrollTo(index)
                    })
                }
                

            }
            WordNavigation(wordNavigationtappedIndex: $wordNavigationtappedIndex)
        }
        
        
   
    }
}

struct MailListView_Previews: PreviewProvider {
    static var previews: some View {
        MailListView()
    }
}
