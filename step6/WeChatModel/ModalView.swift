//
//  ModalView.swift
//  WeChatModel
//
//  Created by 葛鹏 on 2021/8/5.
//

import SwiftUI

struct ModalView: View {
    
    @State var sheetIsPresented = false
    
    var body: some View {
        Button(action: {
            sheetIsPresented = true
        }, label: {
            Text("打开遮罩层")
        }).sheet(isPresented: $sheetIsPresented, content: {
            ZStack{
                Rectangle().fill().foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                Button(action: {
                    sheetIsPresented = false
                }, label: {
                    Text("返回").foregroundColor(.white)
                })
            }
           
        })
    }
}

struct ModalView_Previews: PreviewProvider {
    static var previews: some View {
        ModalView()
    }
}
